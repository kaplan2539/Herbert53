package main


import (
    "log"
    "os"

    "github.com/gotk3/gotk3/gdk"
    "github.com/gotk3/gotk3/glib"
    "github.com/gotk3/gotk3/gtk"
    "fmt"

    "./radio"
)


var key_pressed bool = false
var direction float64 = 0.0
var bar_left  *gtk.ProgressBar = nil
var bar_right *gtk.ProgressBar = nil


func on_key_pressed( win *gtk.ApplicationWindow, ev *gdk.Event ) {
    keyEvent := &gdk.EventKey{ev}
    fmt.Printf("key pressed! %v\n",keyEvent)

    switch key:=keyEvent.KeyVal(); key {
        case gdk.KEY_Left:
            if direction > -1 { direction -= 0.1 }
            key_pressed=true
        case gdk.KEY_Right:
            if direction < +1 { direction += 0.1 }
            key_pressed=true
    }

    bar_left.SetFraction(-direction)
    bar_right.SetFraction(direction)
}

func on_activate( application *gtk.Application) {
    grid, err := gtk.GridNew()
    if err != nil {
        log.Fatal("Unable to create grid:", err)
    }
    grid.SetOrientation(gtk.ORIENTATION_VERTICAL)

    bar_left, err = gtk.ProgressBarNew()
    if err != nil {
        log.Fatal("Unable to create progress bar:", err)
    }
    bar_left.SetInverted(true)
    bar_left.SetHExpand(true)
    grid.Add(bar_left)

    bar_right, err = gtk.ProgressBarNew()
    if err != nil {
        log.Fatal("Unable to create progress bar:", err)
    }
    bar_right.SetInverted(false)
    bar_right.SetHExpand(true)
    grid.AttachNextTo(bar_right, bar_left, gtk.POS_RIGHT, 1, 1)

    // Create ApplicationWindow
    appWindow, err := gtk.ApplicationWindowNew(application)
    if err != nil {
        log.Fatal("Could not create application window.", err)
    }

    appWindow.Add(&grid.Container.Widget)
    appWindow.Connect("key-press-event", on_key_pressed)
    appWindow.SetTitle("Basic Application.")
    appWindow.SetDefaultSize(400, 400)
    appWindow.ShowAll()
}

func decay() bool {
    if(key_pressed) {
        key_pressed=false
    } else {
        direction *= 0.95
        bar_left.SetFraction(-direction)
        bar_right.SetFraction(direction)
    }
    glib.TimeoutAdd(10,decay)

    radio.Send(direction)

    return true
}

func main() {

    //connect(broker)
    radio.Connect()


    const appID = "de.eadc0.d"
    application, err := gtk.ApplicationNew(appID, glib.APPLICATION_FLAGS_NONE);
    if err != nil {
        log.Fatal("Could not create application.", err)
    }

    glib.TimeoutAdd(10,decay)
    application.Connect("activate", func() { on_activate(application) })
    application.Run(os.Args)
}

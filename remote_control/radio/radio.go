package radio

import (
	"crypto/tls"
	_ "flag"
	"fmt"
	"os"
	"strconv"
	_ "syscall"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
)

var client MQTT.Client = nil

func on_direction(client MQTT.Client, message MQTT.Message) {
}

func on_alive_received(client MQTT.Client, message MQTT.Message) {
    qos := byte(0)
    retained := false

    if(string(message.Payload())=="ping") {
	    fmt.Printf("Received: %s\n", message.Payload())
	    fmt.Printf("Sending: pong\n")
        client.Publish(message.Topic(), qos, retained, "pong")
    } else {
	    fmt.Printf("Received: %s\n", message.Payload())
    }
}

var i int64

func on_connection_lost(client MQTT.Client, reason error) {
	fmt.Printf("Connection lost.\n");
	fmt.Printf("Reconnect?\n");
}

func Connect() {
	hostname, _ := os.Hostname()

	server := "tcp://127.0.0.1:1883"
	qos := 0

	connOpts := &MQTT.ClientOptions{
		ClientID:             hostname+strconv.Itoa(time.Now().Second()),
		CleanSession:         true,
		Username:             "",
		Password:             "",
        AutoReconnect:        true,
		MaxReconnectInterval: 1 * time.Second,
		KeepAlive:            60,
		TLSConfig:            tls.Config{InsecureSkipVerify: true, ClientAuth: tls.NoClientCert},
        OnConnectionLost:     on_connection_lost,
	}
	connOpts.AddBroker(server)
	connOpts.OnConnect = func(c MQTT.Client) {
		if token := c.Subscribe("alive", byte(qos), on_alive_received); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
		if token := c.Subscribe("direction", byte(qos), on_direction); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}

	client = MQTT.NewClient(connOpts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	} else {
		fmt.Printf("Connected to %s\n", server)
	}
}

func Send(val float64) {
    client.Publish("direction",0,false,fmt.Sprintf("%f",val))
}

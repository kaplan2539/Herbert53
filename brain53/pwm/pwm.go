package pwm

import (
    "os"
    "io/ioutil"
    "fmt"
    "strconv"
)

const PWMCHIP0_EXPORT_PATH = "/sys/class/pwm/pwmchip0/export"

const PWM_FMT      = "/sys/class/pwm/pwmchip0/pwm%d"
const PERIOD_FMT   = "/sys/class/pwm/pwmchip0/pwm%d/period"
const DUTY_FMT     = "/sys/class/pwm/pwmchip0/pwm%d/duty_cycle"
const ENABLE_FMT   = "/sys/class/pwm/pwmchip0/pwm%d/enable"
const POLARITY_FMT = "/sys/class/pwm/pwmchip0/pwm%d/polarity"

func file_exists(path string) bool {
    if _, err := os.Stat(path); os.IsNotExist(err) {
        return false
    }
    return true
}

func export( idx int ) {
    path:=fmt.Sprintf(PWM_FMT,idx)
    if ! file_exists(path) {
        fmt.Printf( "%s does not exist\n", path )
        if err:=ioutil.WriteFile(PWMCHIP0_EXPORT_PATH,[]byte(strconv.Itoa(idx)),0644); err!=nil {
            panic(err)
        }
    } else {
        fmt.Printf( "%s does exist\n", path )
    }
}

func Init() {
    export(0)
    export(1)
    SetPolarity(0,0)
    SetPolarity(1,0)
}

func write_val(path string, value int) {
    if ! file_exists(path) {
        panic(fmt.Sprintf("%s does not exist\n", path))
    } else {
        if err:=ioutil.WriteFile(path,[]byte(strconv.Itoa(value)),0644); err!=nil {
            panic(err)
        }
    }
}

func write_str_val(path string, value string) {
    if ! file_exists(path) {
        panic(fmt.Sprintf("%s does not exist\n", path))
    } else {
        if err:=ioutil.WriteFile(path,[]byte(value),0644); err!=nil {
            panic(err)
        }
    }
}

// 0: normal
// 1: inverted
func SetPolarity(pwm_idx int, polarity int) {
    path:=fmt.Sprintf(POLARITY_FMT,pwm_idx)

    Disable(pwm_idx)

    if polarity == 1 {
        write_str_val(path,"inverted")
    } else {
        write_str_val(path,"normal")
    }
}

func SetPeriod(pwm_idx int, period int) {
    path:=fmt.Sprintf(PERIOD_FMT,pwm_idx)
    write_val(path,period)
}

func SetDutyCycle(pwm_idx int, duty int) {
    path:=fmt.Sprintf(DUTY_FMT,pwm_idx)
    write_val(path,duty)
}

func Enable(pwm_idx int) {
    path:=fmt.Sprintf(ENABLE_FMT,pwm_idx)
    write_val(path,1)
}

func Disable(pwm_idx int) {
    path:=fmt.Sprintf(ENABLE_FMT,pwm_idx)
    write_val(path,0)
}

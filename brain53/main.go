package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strconv"
	"syscall"
    "time"
	MQTT "github.com/eclipse/paho.mqtt.golang"
    "./pwm"
)

var i int64
var last_pong time.Time
var last_set time.Time
var last_val int = 100


func onExit() {
    fmt.Printf("Bye bye.\n");
    os.Exit(0)
}

func onMessageReceived(client MQTT.Client, message MQTT.Message) {
	fmt.Printf("direction %s\n", message.Payload())

    val,err := strconv.ParseFloat(string(message.Payload()),64)
    if err == nil {
        if val < -0.9 { val = -0.9 }
        if val >  0.9 { val =  0.9 }
        fmt.Printf("val=%f",val)
        val *= -400000
        val += 1500000
//        if val < 0.0 { val = 0.0 }
//        if val > 1000.0 { val = 1000.0 }
        fmt.Printf("val=%d\n",int(val))

        if int(val) != last_val && time.Since(last_set)>(time.Millisecond*50){
            pwm.SetDutyCycle(0,int(val))
            last_val=int(val)
            last_set = time.Now()
        }
    }
}

func onAliveReceived(client MQTT.Client, message MQTT.Message) {
    if(string(message.Payload())=="pong") {
	    //fmt.Printf("last_pong==%v\n", last_pong )
        last_pong = time.Now()
	    //fmt.Printf("Received: %s (%v)\n", message.Payload(), last_pong )
    }
}

func onConnectionLost(client MQTT.Client, reason error) {
	fmt.Printf("Connection lost.\n");
	fmt.Printf("Reconnect?\n");
//    onExit()
}

func main() {
	c := make(chan os.Signal, 1)
	i = 0
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		fmt.Println("signal received, exiting")
		os.Exit(0)
	}()

    pwm.Init()
    pwm.Enable(1)
    pwm.SetPeriod(1,  20000000)
    pwm.SetDutyCycle(1,1500000)
    pwm.Enable(0)
    pwm.SetPeriod(0,  20000000)
    pwm.SetDutyCycle(0,1500000)


	hostname, _ := os.Hostname()

	server := flag.String("server", "tcp://192.168.81.2:1883",
                          "The full url of the MQTT server to connect to ex: tcp://127.0.0.1:1883")
	//topic := flag.String("topic", "#", "Topic to subscribe to")
	qos := flag.Int("qos", 0, "The QoS to subscribe to messages at")
	clientid := flag.String("clientid", hostname+strconv.Itoa(time.Now().Second()),
                            "A clientid for the connection")
	username := flag.String("username", "", "A username to authenticate to the MQTT server")
	password := flag.String("password", "", "Password to match username")
	flag.Parse()

	connOpts := &MQTT.ClientOptions{
		ClientID:             *clientid,
		CleanSession:         true,
		Username:             *username,
		Password:             *password,
        AutoReconnect:        true,
		MaxReconnectInterval: 1 * time.Second,
		KeepAlive:            60,
		TLSConfig:            tls.Config{InsecureSkipVerify: true, ClientAuth: tls.NoClientCert},
        OnConnectionLost:     onConnectionLost,
	}
	connOpts.AddBroker(*server)
	connOpts.OnConnect = func(c MQTT.Client) {
		if token := c.Subscribe("direction", byte(*qos), onMessageReceived); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
		if token := c.Subscribe("alive", byte(*qos), onAliveReceived); token.Wait() && token.Error() != nil {
			panic(token.Error())
		}
	}

	client := MQTT.NewClient(connOpts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	} else {
		fmt.Printf("Connected to %s\n", *server)
	}

    last_pong = time.Now()
    last_set = time.Now()

    ticker := time.NewTicker(time.Millisecond * 500)
    go func () {
        for _ = range ticker.C {
            client.Publish("alive", byte(*qos), false, "ping")
        }
    }()

	for {
		time.Sleep(300 * time.Millisecond)
        if time.Since(last_pong) > time.Second {
            fmt.Printf("Houston, we have un problemo: %v\n",time.Since(last_pong))
        }
	}
}
